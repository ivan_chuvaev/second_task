'use strict';

import {Request, ResponseToolkit} from "@hapi/hapi";
import {Student} from "./students_model";
import {getObjectWithExactProperties} from "./utils";

export async function getGradeHandler (request: Request, reply: ResponseToolkit) {

    return await Student.aggregate('averageGrade', request.query.grade, {
        where: getObjectWithExactProperties(request.query, ['faculty', 'sex'])
    })

}

export async function listStudentsHandler(request: Request, reply: ResponseToolkit){

    let limit = request.query.resultsOnPage ? request.query.resultsOnPage : 50
    
    let students:Student[] = await Student.findAll({
        where: getObjectWithExactProperties(request.query, ['faculty']),
        order: [['averageGrade', (request.query.sort == 'gradeDown') ? 'DESC' : 'ASC']],
        limit: limit,
        offset: request.query.page ? request.query.page * limit : 0
    });

    if(!students)
        return reply.response('database is empty').code(500);

    return students;

}

export async function studentIdHandler(request: Request, reply: ResponseToolkit) {
            
    const student = await Student.findOne({
        where: {id:request.params.id}
    })

    if(!student)
        return reply.response(`student ${request.params.id} not found`).code(404);

    return student;

}

export async function studentIdEditHandler(request: Request, reply: ResponseToolkit){

    let payload:any = request.payload
    
    const student = await Student.findOne({
        where: {id:request.params.id}
    })

    if(!student)
        return reply.response(`student ${request.params.id} not found`).code(404);

    Object.keys(student.dataValues).forEach(key=>{

        if(payload[key])
            student[key] = payload[key]

    })
    await student.save();

    return student;
}