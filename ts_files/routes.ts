'use strict';
import * as handlers from './handlers';
import * as schemas from './schemas';

export const routes = [

    //this route will return only one number
    {
        method: 'GET',
        path: '/get_grade',
        options:{
            validate:{
                query: schemas.getGradeShema
            }
        },
        handler: handlers.getGradeHandler
    },

    //faculty's students with grade ordering
    {
        method: 'GET',
        path: '/students',
        options:{
            validate:{
                query: schemas.studentsByFacultySchema
            }
        },
        handler: handlers.listStudentsHandler
    },

    //student info page
    {
        method: 'GET',
        path: '/student/{id}',
        options: {
            validate: {
                params: schemas.idParamSchema
            }
        },
        handler: handlers.studentIdHandler
    },

    //edit student info
    {
        method: 'POST',
        path: '/student/{id}',
        options: {
            validate: {
                params: schemas.idParamSchema,
                payload: schemas.editStudentSchema
            }
        },
        handler: handlers.studentIdEditHandler
    }
]