import { Sequelize } from "sequelize/types";

export async function bd_connection_test(sequelize: Sequelize):Promise<boolean>{
    try {

        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        return true;

      } catch (error) {

        console.error('Unable to connect to the database:', error);
        return false;

      }
}