'use strict';

import {Server} from "@hapi/hapi";
import * as path from "path";
import {initStudentModel, Student} from "./students_model";
import {fillStudendsTable} from "./students_model_filling";
import {routes} from "./routes";
import * as bdt from "./bd_test";
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('postgres://postgres:123321@localhost:5432/crypton_bd')
sequelize.options.logging = false;

const init = async () => {

    const server = new Server({
        port: 3000,
        host: 'localhost',
        routes: {
            files: {
                relativeTo: path.join(__dirname, 'public')
            }
        }
    });

    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
    
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);

});

(async ()=>{
    if(await bdt.bd_connection_test(sequelize)){

        initStudentModel(sequelize);
        await sequelize.sync();

        if(Student.count() == 0)
            await fillStudendsTable();

        init();
    }
})();