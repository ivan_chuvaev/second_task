'use strict';

import * as utils from './utils';
import * as sm from './students_model';

const studentsCount = 100;

const firstNames:string[] = [
    'Liam','Noah','Oliver','Elijah','William','James','Benjamin','Lucas','Henry','Alexander','Mason','Michael',
    'Ethan','Daniel','Jacob','Logan','Jackson','Levi','Sebastian','Mateo','Jack','Owen','Theodore','Aiden',
    'Samuel','Joseph','John','David','Wyatt','Matthew','Luke','Asher','Carter','Julian','Grayson','Leo',
    'Jayden','Gabriel','Isaac','Lincoln','Anthony','Hudson','Dylan','Ezra','Thomas','Charles','Christopher',
    'Jaxon','Maverick','Josiah','Isaiah','Andrew','Elias','Joshua','Nathan','Caleb','Ryan'
]
const lastNames:string[] = [
    'Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Garcia', 'Miller', 'Davis', 'Rodriguez', 'Martinez',
    'Hernandez', 'Lopez', 'Gonzalez', 'Wilson', 'Anderson', 'Thomas', 'Taylor', 'Moore', 'Jackson', 'Martin',
    'Lee', 'Perez', 'Thompson', 'White', 'Harris', 'Sanchez', 'Clark', 'Ramirez', 'Lewis', 'Robinson', 'Walker',
    'Young', 'Allen', 'King', 'Wright', 'Scott', 'Torres', 'Nguyen', 'Hill', 'Flores', 'Green', 'Adams', 'Nelson',
    'Baker', 'Hall', 'Rivera', 'Campbell', 'Mitchell', 'Carter', 'Roberts'
]

export async function fillStudendsTable(){

    let studentsArray:Promise<sm.Student>[] = new Array(studentsCount);

    for(let i = 0; i < studentsCount; i++){
        studentsArray[i] = sm.Student.create({

            firstName: firstNames[utils.randInt(firstNames.length-1)],
            lastName: lastNames[utils.randInt(lastNames.length-1)],
            sex: sm.sex[utils.randInt(sm.sex.length-1)],
            phone: utils.randNumberString(sm.phoneLength),
            faculty: sm.faculties_arr[utils.randInt(sm.faculties_arr.length-1)],
            averageGrade: Math.random()*sm.maxGrade 

        })
    }

    await Promise.all(studentsArray);
};