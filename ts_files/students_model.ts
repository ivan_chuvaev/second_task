'use strict';

import { Sequelize } from 'sequelize';
const { Model, DataTypes } = require('sequelize');


export const sex:string[]=['male', 'female'];

export const faculties_arr:string[] = [
    'Faculty of radio engineering', 'Faculty of radio design',
    'Faculty of computer systems', 'Faculty of control systems',
    'Faculty of electronic engineering', 'Faculty of economics',
    'Faculty of innovation technologies', 'Faculty of human sciences',
    'Faculty of law', 'Faculty of security',
    'Faculty of distance learning', 'Faculty of extramural and evening education',
    'Faculty of advanced training'
];

export const maxGrade:number = 5;

export const phoneLength:number = 11;

export class Student extends Model {}
export function initStudentModel(sequelize:Sequelize, modelName: string = 'students'):void{
    Student.init({
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: true
            },
        lastName: {
            type: DataTypes.STRING,
            allowNull: true
        },
        sex: {
            type: DataTypes.ENUM({values: sex}),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true
        },
        faculty:{
            type: DataTypes.ENUM({values: faculties_arr}),
            allowNull: true
        },
        averageGrade:{
            type: DataTypes.NUMERIC(3,2),
            allowNull: true
        }
    }, { 
        sequelize, modelName: modelName
    });
}