export function randNumberString(length:number):string{
    let ret:string = '';
    for(let i = 0; i < length; i++){
        ret += Math.floor(Math.random()*10).toString();
    }
    return ret
}

export function randInt(max:number):number{
    return Math.floor(Math.random()*(max+1));
}

export function getObjectWithExactProperties(inputOjb:any, props:string[]){

    let arr:object[] = new Array<object>(props.length);
    props.forEach((element:string, index:number)=>{
        arr[index] = inputOjb[element] ? { [element]: inputOjb[element] } : undefined;
    });

    return Object.assign({}, ...arr)
    
}