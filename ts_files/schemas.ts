'use strict';
import * as Joi from 'joi';
import * as sm from './students_model';

export const idParamSchema = Joi.object({

    id: Joi.string().guid()

})

export const editStudentSchema = Joi.object({

    firstName: Joi.string(),
    lastName: Joi.string(),
    sex: Joi.string().valid(...sm.sex),
    faculty: Joi.string().valid(...sm.faculties_arr),
    phone: Joi.string().pattern(/^[0-9]+$/),
    averageGrade: Joi.number().positive().max(sm.maxGrade)

})

export const getGradeShema = Joi.alternatives().try(

    Joi.object({
        faculty: Joi.string().valid(...sm.faculties_arr).required(),
        grade: Joi.string().valid('avg', 'max', 'min').required()
    }),

    Joi.object({
        sex: Joi.string().valid(...sm.sex).required(),
        grade: Joi.string().valid('avg', 'max', 'min').required()
    })

)

export const studentsByFacultySchema = Joi.object({

    faculty: Joi.string().valid(...sm.faculties_arr).required(),
    sort: Joi.string().valid('gradeUp', 'gradeDown'),
    resultsOnPage: Joi.number().valid(10,20,50,100),
    page: Joi.number().integer().min(0)

})