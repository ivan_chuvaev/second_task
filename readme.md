to run this project you need to install postgresql,  
change password of user postgres to '123321',  
create database with name 'crypton_bd'  
and run postgresql on port 5432  

after that move to project directory and run following commands:  
npm install  
npm start

grade by faculty:  

&ensp;&ensp;average:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=avg&faculty=Faculty%20of%20security  
&ensp;&ensp;max:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=max&faculty=Faculty%20of%20security  
&ensp;&ensp;min:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=min&faculty=Faculty%20of%20security 

grade by gender:  

&ensp;&ensp;average:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=avg&sex=male   
&ensp;&ensp;max:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=max&sex=male   
&ensp;&ensp;min:
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=min&sex=male   

sort students by grades down:  
http://127.0.0.1:3000/students?sort=gradeDown&faculty=Faculty%20of%20law  

student info by id:  
http://127.0.0.1:3000/student/46586f9f-eb52-4629-8d10-24fbd9dd8ed0  

edit student with post request:  
http://127.0.0.1:3000/student/f5a4daa0-3216-462f-8f21-8670cddcd74d/edit  

